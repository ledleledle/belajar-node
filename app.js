const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv/config');

//Middlewares
app.use(cors());
app.use(bodyParser.json());

//Import Routes
const postRoute = require('./routes/posts');

app.use('/posts', postRoute);

//Routes
app.get('/', (req, res) => {
    res.send('This is Home');
});

//DB
mongoose.connect(
    process.env.DB_CONN,
    { useNewUrlParser: true, useUnifiedTopology: true },
    () => console.log('connected to db')
);

//App listen to server
app.listen('3000');